# vue-dynamic-loading
## About
### Description
This is Vue.js code for the Hackathon that required an application to:
* load components dynamically based on a json file
* pick the best component based on certain factors
* etc...
#### This project also requires the server to be running found [here](https://bitbucket.org/namir_alcumusgroup/vue-dynamic-loading-server/src/master/)
### About the code
* Components can be represented as .vue or a .js file
* Components are registered globally in the main.js file. It shows the various ways components can be loaded, including asynchronously. I have loaded each component individually, but it can also be loaded automatically based on a location. See [here](https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components).
* Components are displayed dynamically in Home.vue using the component tag
* The components are also listed on the server in a file called components-list.json, each of which have an array that contains a list of keywords with a "name" and "priority". These two fields together describe an attribute that the component has and how well it can do it. The front end (in form-loader-service.js) then checks this list when a form is loaded to see which component is best suited for the job.

### Important notes
* One thing I spent a lot of time attempting is loading a component from a remote location. On the server you will see a few .vue and .js components which I tried to load into the application. Although the TextField component is asynchronously loaded with a timeout, I could't figuire out how to load it externally.
* One thing I would like to say is that everything in this application I grabbed from the docs on Vue.js website. 
    * https://vuejs.org/v2/guide/components-dynamic-async.html
    * https://vuejs.org/v2/guide/components-registration.html
* All this funcitonality is built into Vue.js - I didn't have to use any special libraries.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
