import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

import App from './App.vue';
import router from './router';

import './registerServiceWorker';

Vue.config.productionTip = false;

Vue.component('DynamicComponent', {
  template: '<div>This is a dynamic component</div>',
});

Vue.component('TextField', (resolve, reject) => {
  setTimeout(() => {
    resolve({
      template: '<div class="text-field"><label>{{name}} {{ question }}</label><input type="text" v-model="message"/><p>{{message}}</p></div>',
      name: 'component',
      props: {
        question: String,
        name: String,
      },
      data() {
        return {
          message: '',
        };
      },
    });
  }, 5000);
});

Vue.component('HelloWorld', (resolve) => {
  const loadImport = import('./assets/hello-world.js');
  loadImport.then((data) => {
    resolve(data);
  });
});

Vue.component('SelectOption', (resolve) => {
  require(['./components/SelectOption.vue'], resolve);
});
/*
const requireComponent = require.context(
  // Look for files in the current directory
  './components',
  // Do not look in subdirectories
  true,
  // Only include "_base-" prefixed .vue files
 // /_base-[\w-]+\.vue$/
)

// For each matching file name...
requireComponent.keys().forEach(fileName => {
  // Get the component config
  const componentConfig = requireComponent(fileName)
  // Get the PascalCase version of the component name
  const componentName = upperFirst(
    camelCase(
      fileName
        // Remove the "./_" from the beginning
        .replace(/^\.\/_/, '')
        // Remove the file extension from the end
        .replace(/\.\w+$/, '')
    )
  )
  console.log(componentName);
  // Globally register the component
  Vue.component(componentName, componentConfig.default || componentConfig)
})
*/

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
