import form from '@/assets/form.json';
import axios from 'axios';
import { detect } from 'detect-browser';

const browser = detect();

export class FormLoaderService {
  static initialised() {
    return !!FormLoaderService.instance;
  }

  static getInstance() {
    if (!FormLoaderService.initialised()) {
      FormLoaderService.instance = new FormLoaderService();
    }
    return FormLoaderService.instance;
  }

  evaluateBestComponent(question) {
    let bestComponentScore = 0;
    let bestComponentName = this.componentsList[0].name;

    this.componentsList.forEach((component) => {
      const componentKeywords = component.keywords;
      const keywordsToCheck = [question.type, browser.name];

      let score = 0;

      keywordsToCheck.forEach((keyword) => {
        score += this.getScoreFromKeywords(componentKeywords, keyword);
      });

      if (score > bestComponentScore) {
        bestComponentName = component;
        bestComponentScore = score;
      }
    });
    return bestComponentName.name;
  }

  getScoreFromKeywords(keywordsArray, keyword) {
    const browserObj = keywordsArray.find(e => e.name === keyword);
    if (browserObj) {
      return browserObj.priority;
    }
    return 0;
  }

  getQuestions() {
    return new Promise(((resolve, reject) => {
      axios.get('http://localhost:3000/json/components-list.json')
        .then((result) => {
          this.componentsList = result.data.components;
          axios.get('http://localhost:3000/json/form2.json')
            .then((result) => {
              result.data.sections.forEach((section) => {
                section.questions.forEach((q) => {
                  q.type = this.evaluateBestComponent(q);
                });
              });
              resolve(result.data);
            });
        });
    }));
  }
}
