export default {
  template: '<div class="hello"><h1>{{ msg }}</h1></div></template>',
  name: 'HelloWorld',
  props: {
    msg: String,
  },
};
