module.exports = {
  configureWebpack: {
    resolve: {
      alias: { vue$: 'vue/dist/vue.esm.js' }, // https://vuejs.org/v2/guide/installation.html#Runtime-Compiler-vs-Runtime-only
    },
  },
};
